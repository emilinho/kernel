### Creación del paquete `deb` del  *kernel*

Ejecutar `make deb-pkg` para generar los paquetes necesarios para instalar este kernel en un equipo con sistema operativo de tipo _debian_

```sh
tonejito@debian:/mnt$ cd linux-3.16.50
tonejito@debian:/mnt/linux-3.16.50$ make deb-pkg
  CHK     include/config/kernel.release
make KBUILD_SRC=
  CHK     include/config/kernel.release
  CHK     include/generated/uapi/linux/version.h
  CHK     include/generated/utsrelease.h
  CALL    scripts/checksyscalls.sh
  CHK     include/generated/compile.h
  PASYMS  arch/x86/realmode/rm/pasyms.h
  LDS     arch/x86/realmode/rm/realmode.lds
  LD      arch/x86/realmode/rm/realmode.elf
  RELOCS  arch/x86/realmode/rm/realmode.relocs
  OBJCOPY arch/x86/realmode/rm/realmode.bin
  AS      arch/x86/realmode/rmpiggy.o
  LD      arch/x86/realmode/built-in.o
  LD      arch/x86/built-in.o
  LINK    vmlinux
  LD      vmlinux.o
  MODPOST vmlinux.o
  GEN     .version
  CHK     include/generated/compile.h
  UPD     include/generated/compile.h
  CC      init/version.o
  LD      init/built-in.o
  KSYM    .tmp_kallsyms1.o
  KSYM    .tmp_kallsyms2.o
  LD      vmlinux
  SORTEX  vmlinux
  SYSMAP  System.map
  VOFFSET arch/x86/boot/voffset.h
  OBJCOPY arch/x86/boot/compressed/vmlinux.bin
  XZKERN  arch/x86/boot/compressed/vmlinux.bin.xz
  MKPIGGY arch/x86/boot/compressed/piggy.S
  AS      arch/x86/boot/compressed/piggy.o
  LD      arch/x86/boot/compressed/vmlinux
  ZOFFSET arch/x86/boot/zoffset.h
  AS      arch/x86/boot/header.o
  CC      arch/x86/boot/version.o
  LD      arch/x86/boot/setup.elf
  OBJCOPY arch/x86/boot/setup.bin
  OBJCOPY arch/x86/boot/vmlinux.bin
  BUILD   arch/x86/boot/bzImage
Setup is 17404 bytes (padded to 17408 bytes).
System is 3066 kB
CRC b0598404
Kernel: arch/x86/boot/bzImage is ready  (#2)
  Building modules, stage 2.
  MODPOST 3046 modules
  BUILDDEB
  INSTALL arch/x86/crypto/aes-x86_64.ko
	...
  INSTALL arch/x86/oprofile/oprofile.ko
  INSTALL crypto/ablk_helper.ko
	...
  INSTALL crypto/zlib.ko
  INSTALL drivers/acpi/ac.ko
	...
  INSTALL drivers/xen/xenfs/xenfs.ko
  INSTALL fs/9p/9p.ko
	...
  INSTALL fs/xfs/xfs.ko
  INSTALL lib/bch.ko
	...
  INSTALL lib/ts_kmp.ko
  INSTALL mm/hwpoison-inject.ko
  INSTALL net/802/garp.ko
	...
  INSTALL net/xfrm/xfrm_user.ko
  INSTALL sound/ac97_bus.ko
	...
  INSTALL sound/usb/usx2y/snd-usb-usx2y.ko
  INSTALL debian/tmp/lib/firmware/acenic/tg1.bin
  INSTALL debian/tmp/lib/firmware/acenic/tg2.bin
	...
  INSTALL debian/tmp/lib/firmware/yam/9600.bin
  DEPMOD  3.16.50so-ciencias-unam
  CHK     include/generated/uapi/linux/version.h
  HOSTCC  scripts/unifdef
  INSTALL usr/include/asm-generic/ (35 files)
  INSTALL usr/include/drm/ (18 files)
  INSTALL usr/include/linux/byteorder/ (2 files)
	...
  INSTALL usr/include/linux/ (394 files)
	...
  INSTALL usr/include/xen/ (4 files)
  INSTALL usr/include/uapi/ (0 file)
  INSTALL usr/include/asm/ (64 files)
  CHECK   usr/include/asm-generic/ (35 files)
  CHECK   usr/include/drm/ (18 files)
  CHECK   usr/include/linux/byteorder/ (2 files)
  CHECK   usr/include/linux/ (394 files)
	...
  CHECK   usr/include/xen/ (4 files)
  CHECK   usr/include/uapi/ (0 files)
  CHECK   usr/include/asm/ (64 files)
  CHK     include/generated/uapi/linux/version.h
  INSTALL debian/headertmp/usr/include/asm-generic/ (35 files)
  INSTALL debian/headertmp/usr/include/drm/ (18 files)
  INSTALL debian/headertmp/usr/include/linux/byteorder/ (2 files)
	...
  INSTALL debian/headertmp/usr/include/uapi/ (0 file)
  INSTALL debian/headertmp/usr/include/asm/ (64 files)
dpkg-gencontrol: warning: -isp is deprecated; it is without effect
dpkg-deb: building package `linux-firmware-image-3.16.50so-ciencias-unam' in `../linux-firmware-image-3.16.50so-ciencias-unam_3.16.50so-ciencias-unam-2_amd64.deb'.
dpkg-deb: building package `linux-headers-3.16.50so-ciencias-unam' in `../linux-headers-3.16.50so-ciencias-unam_3.16.50so-ciencias-unam-2_amd64.deb'.
dpkg-deb: building package `linux-libc-dev' in `../linux-libc-dev_3.16.50so-ciencias-unam-2_amd64.deb'.
dpkg-deb: building package `linux-image-3.16.50so-ciencias-unam' in `../linux-image-3.16.50so-ciencias-unam_3.16.50so-ciencias-unam-2_amd64.deb'.
dpkg-deb: building package `linux-image-3.16.50so-ciencias-unam-dbg' in `../linux-image-3.16.50so-ciencias-unam-dbg_3.16.50so-ciencias-unam-2_amd64.deb'.
```

Se puede comprobar que `make` no generó error al revisar el código de salida:

```sh
tonejito@debian:~$ echo $?
0
```

Listar los paquetes generados y tomar nota del tamaño, especialmente para el paquete `linux-image-*-dbg_*.deb`

```sh
tonejito@debian:/mnt/linux-3.16.50$ cd ../
tonejito@debian:/mnt$ ls -larth *.deb
-rw-r--r--  1 tonejito users 945K Nov 21 20:33 linux-firmware-image-3.16.50so-ciencias-unam_3.16.50so-ciencias-unam-2_amd64.deb
-rw-r--r--  1 tonejito users 6.3M Nov 21 20:34 linux-headers-3.16.50so-ciencias-unam_3.16.50so-ciencias-unam-2_amd64.deb
-rw-r--r--  1 tonejito users 749K Nov 21 20:34 linux-libc-dev_3.16.50so-ciencias-unam-2_amd64.deb
-rw-r--r--  1 tonejito users  33M Nov 21 20:38 linux-image-3.16.50so-ciencias-unam_3.16.50so-ciencias-unam-2_amd64.deb
-rw-r--r--  1 tonejito users 315M Nov 21 21:31 linux-image-3.16.50so-ciencias-unam-dbg_3.16.50so-ciencias-unam-2_amd64.deb
```

Obtener la información de control sobre cada paquete

+ `libc-dev`

```sh
tonejito@debian:/mnt$ dpkg --info linux-libc-dev_3.16.50so-ciencias-unam-2_amd64.deb

 new debian package, version 2.0.
 size 766634 bytes: control archive=22981 bytes.
     481 bytes,    13 lines      control              
   52278 bytes,   799 lines      md5sums              
 Package: linux-libc-dev
 Source: linux-upstream
 Version: 3.16.50so-ciencias-unam-2
 Architecture: amd64
 Maintainer: Anonymous <root@debian.local>
 Installed-Size: 3566
 Provides: linux-kernel-headers
 Section: devel
 Priority: optional
 Homepage: http://www.kernel.org/
 Description: Linux support headers for userspace development
  This package provides userspaces headers from the Linux kernel.
  These headers are used by the installed headers for GNU glibc and other system libraries.
```

+ `linux-firmware`

```sh
tonejito@debian:/mnt$ dpkg --info linux-firmware-image-3.16.50so-ciencias-unam_3.16.50so-ciencias-unam-2_amd64.deb
 new debian package, version 2.0.
 size 967592 bytes: control archive=4083 bytes.
     419 bytes,    11 lines      control              
   10901 bytes,   120 lines      md5sums              
 Package: linux-firmware-image-3.16.50so-ciencias-unam
 Source: linux-upstream
 Version: 3.16.50so-ciencias-unam-2
 Architecture: amd64
 Maintainer: Anonymous <root@debian.local>
 Installed-Size: 2157
 Section: kernel
 Priority: optional
 Homepage: http://www.kernel.org/
 Description: Linux kernel firmware, version 3.16.50so-ciencias-unam
  This package contains firmware from the Linux kernel, version 3.16.50so-ciencias-unam.
```

+ `linux-headers`

```sh
tonejito@debian:/mnt$ dpkg --info linux-headers-3.16.50so-ciencias-unam_3.16.50so-ciencias-unam-2_amd64.deb

 new debian package, version 2.0.
 size 6579214 bytes: control archive=260341 bytes.
     516 bytes,    14 lines      control              
 1327246 bytes, 12037 lines      md5sums              
 Package: linux-headers-3.16.50so-ciencias-unam
 Source: linux-upstream
 Version: 3.16.50so-ciencias-unam-2
 Architecture: amd64
 Maintainer: Anonymous <root@debian.local>
 Installed-Size: 50364
 Provides: linux-headers, linux-headers-2.6
 Section: kernel
 Priority: optional
 Homepage: http://www.kernel.org/
 Description: Linux kernel headers for 3.16.50so-ciencias-unam on amd64
  This package provides kernel header files for 3.16.50so-ciencias-unam on amd64.
  This is useful for people who need to build external modules
```

+ `linux-image`

```sh
tonejito@debian:/mnt$ dpkg --info linux-image-3.16.50so-ciencias-unam_3.16.50so-ciencias-unam-2_amd64.deb

 new debian package, version 2.0.
 size 34251348 bytes: control archive=97535 bytes.
     563 bytes,    14 lines      control              
  332737 bytes,  3062 lines      md5sums              
     317 bytes,    12 lines   *  postinst             #!/bin/sh
     313 bytes,    12 lines   *  postrm               #!/bin/sh
     315 bytes,    12 lines   *  preinst              #!/bin/sh
     311 bytes,    12 lines   *  prerm                #!/bin/sh
 Package: linux-image-3.16.50so-ciencias-unam
 Source: linux-upstream
 Version: 3.16.50so-ciencias-unam-2
 Architecture: amd64
 Maintainer: Anonymous <root@debian.local>
 Installed-Size: 166794
 Suggests: linux-firmware-image-3.16.50so-ciencias-unam
 Provides: linux-image, linux-image-2.6, linux-modules-3.16.50so-ciencias-unam
 Section: kernel
 Priority: optional
 Homepage: http://www.kernel.org/
 Description: Linux kernel, version 3.16.50so-ciencias-unam
  This package contains the Linux kernel, modules and corresponding other files,
  version: 3.16.50so-ciencias-unam.
```

+ `linux-image`, símbolos de depuración

```sh
tonejito@debian:/mnt$ dpkg --info linux-image-3.16.50so-ciencias-unam-dbg_3.16.50so-ciencias-unam-2_amd64.deb

 new debian package, version 2.0.
 size 329307240 bytes: control archive=97264 bytes.
     534 bytes,    13 lines      control              
  374318 bytes,  3049 lines      md5sums              
 Package: linux-image-3.16.50so-ciencias-unam-dbg
 Source: linux-upstream
 Version: 3.16.50so-ciencias-unam-2
 Architecture: amd64
 Maintainer: Anonymous <root@debian.local>
 Installed-Size: 2336907
 Provides: linux-debug, linux-debug-3.16.50so-ciencias-unam
 Section: debug
 Priority: optional
 Homepage: http://www.kernel.org/
 Description: Linux kernel debugging symbols for 3.16.50so-ciencias-unam
  This package will come in handy if you need to debug the kernel.
  It provides all the necessary debug symbols for the kernel and its modules.
```
