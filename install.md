### Instalación de los paquetes del *kernel*

#### Listar los paquetes generados

```sh
tonejito@debian:/mnt/linux-3.16.50$ cd ../
tonejito@debian:/mnt$ ls -larth *.deb
-rw-r--r--  1 tonejito users 945K Nov 21 20:33 linux-firmware-image-3.16.50so-ciencias-unam_3.16.50so-ciencias-unam-2_amd64.deb
-rw-r--r--  1 tonejito users 6.3M Nov 21 20:34 linux-headers-3.16.50so-ciencias-unam_3.16.50so-ciencias-unam-2_amd64.deb
-rw-r--r--  1 tonejito users 749K Nov 21 20:34 linux-libc-dev_3.16.50so-ciencias-unam-2_amd64.deb
-rw-r--r--  1 tonejito users  33M Nov 21 20:38 linux-image-3.16.50so-ciencias-unam_3.16.50so-ciencias-unam-2_amd64.deb
-rw-r--r--  1 tonejito users 315M Nov 21 21:31 linux-image-3.16.50so-ciencias-unam-dbg_3.16.50so-ciencias-unam-2_amd64.deb
```

### Instalar los paquetes en el sistema operativo

#### Elevar privilegios

```sh
tonejito@debian:/mnt$ sudo su -
```

#### Instalar los paquetes con `dpkg`

```sh
root@debian:/mnt# dpkg -i \
  linux-firmware-image-3.16.50so-ciencias-unam_3.16.50so-ciencias-unam-2_amd64.deb \
  linux-headers-3.16.50so-cienciencias-unam-2_amd64.deb \
  linux-image-3.16.50so-ciencias-unam_3.16.50so-ciencias-unam-2_amd64.deb \
;
Selecting previously unselected package linux-image-3.16.50so-ciencias-unam.
(Reading database ... 46396 files and directories currently installed.)
Preparing to unpack linux-image-3.16.50so-ciencias-unam_3.16.50so-ciencias-unam-2_amd64.deb ...
Unpacking linux-image-3.16.50so-ciencias-unam (3.16.50so-ciencias-unam-2) ...
Selecting previously unselected package linux-headers-3.16.50so-ciencias-unam.
Preparing to unpack linux-headers-3.16.50so-ciencias-unam_3.16.50so-ciencias-unam-2_amd64.deb ...
Unpacking linux-headers-3.16.50so-ciencias-unam (3.16.50so-ciencias-unam-2) ...
Selecting previously unselected package linux-firmware-image-3.16.50so-ciencias-unam.
Preparing to unpack linux-firmware-image-3.16.50so-ciencias-unam_3.16.50so-ciencias-unam-2_amd64.deb ...
Unpacking linux-firmware-image-3.16.50so-ciencias-unam (3.16.50so-ciencias-unam-2) ...
Setting up linux-image-3.16.50so-ciencias-unam (3.16.50so-ciencias-unam-2) ...
update-initramfs: Generating /boot/initrd.img-3.16.50so-ciencias-unam
Generating grub configuration file ...
Found linux image: /boot/vmlinuz-3.16.50so-ciencias-unam
Found initrd image: /boot/initrd.img-3.16.50so-ciencias-unam
Found linux image: /boot/vmlinuz-3.16.0-4-amd64
Found initrd image: /boot/initrd.img-3.16.0-4-amd64
done
Setting up linux-headers-3.16.50so-ciencias-unam (3.16.50so-ciencias-unam-2) ...
Setting up linux-firmware-image-3.16.50so-ciencias-unam (3.16.50so-ciencias-unam-2) ...
```

#### Listar los paquetes instalados

Se pueden listar los paquetes del *kernel* utilizando `aptitude`:

```sh
root@debian:/mnt# aptitude search so-ciencias-unam
i   linux-firmware-image-3.16.50so-ciencias-unam  -  Linux kernel firmware, version 3.16.50so-ciencias-unam
i   linux-headers-3.16.50so-ciencias-unam         -  Linux kernel headers for 3.16.50so-ciencias-unam on amd64
i   linux-image-3.16.50so-ciencias-unam           -  Linux kernel, version 3.16.50so-ciencias-unam
v   linux-modules-3.16.50so-ciencias-unam         -
```

Y también se puede utilizar `apt`:

```sh
root@debian:/mnt# apt list --installed '*so-ciencias-unam*'
Listing... Done
linux-firmware-image-3.16.50so-ciencias-unam/now 3.16.50so-ciencias-unam-2 amd64 [installed,local]
linux-headers-3.16.50so-ciencias-unam/now 3.16.50so-ciencias-unam-2 amd64 [installed,local]
linux-image-3.16.50so-ciencias-unam/now 3.16.50so-ciencias-unam-2 amd64 [installed,local]
```

### Verificar el contenido del directorio `/boot`

Dentro del directorio `/boot` deben existir los siguientes archivos para cada *kernel* que se tenga instalado:

+ `vmlinuz` (bzImage)
+ `System.map`
+ `config`
+ `initrd.img`

```sh
root@debian:~# ls -lart /boot/*so-ciencias-unam*
-rw-r--r--  1 root root  3156160 Nov 21 20:26 /boot/vmlinuz-3.16.50so-ciencias-unam
-rw-r--r--  1 root root  2669571 Nov 21 20:26 /boot/System.map-3.16.50so-ciencias-unam
-rw-r--r--  1 root root   157129 Nov 21 20:26 /boot/config-3.16.50so-ciencias-unam
-rw-r--r--  1 root root 15071408 Nov 21 21:51 /boot/initrd.img-3.16.50so-ciencias-unam
root@debian:~# file -s /boot/*so-ciencias-unam*
/boot/vmlinuz-3.16.50so-ciencias-unam:    Linux kernel x86 boot executable bzImage, version 3.16.50so-ciencias-unam (tonejito@debian.local) #2 SMP Tue Nov , RO-rootFS, swap_dev 0x2, Normal VGA
/boot/System.map-3.16.50so-ciencias-unam: ASCII text
/boot/config-3.16.50so-ciencias-unam:     Linux make config build file, ASCII text
/boot/initrd.img-3.16.50so-ciencias-unam: gzip compressed data, last modified: Tue Nov 21 21:51:08 2017, from Unix
```

### Verificar la configuración del cargador de inicio `grub`

Debe existir una configuración de `grub` que haga referencia al *kernel* que se compiló e instaló desde paquetes:

```sh
root@debian:~# grep -B 12 -A 1 so-ciencias-unam /boot/grub/grub.cfg | head -n 17
menuentry 'Debian GNU/Linux' --class debian --class gnu-linux --class gnu --class os $menuentry_id_option 'gnulinux-simple-2fd4fb17-ef7c-4a91-b034-ee8215868d1b' {
        load_video
        insmod gzio
        if [ x$grub_platform = xxen ]; then insmod xzio; insmod lzopio; fi
        insmod part_msdos
        insmod ext2
        set root='hd0,msdos1'
        if [ x$feature_platform_search_hint = xy ]; then
          search --no-floppy --fs-uuid --set=root --hint-bios=hd0,msdos1 --hint-efi=hd0,msdos1 --hint-baremetal=ahci0,msdos1  2fd4fb17-ef7c-4a91-b034-ee8215868d1b
        else
          search --no-floppy --fs-uuid --set=root 2fd4fb17-ef7c-4a91-b034-ee8215868d1b
        fi
        echo    'Loading Linux 3.16.50so-ciencias-unam ...'
        linux   /boot/vmlinuz-3.16.50so-ciencias-unam root=UUID=2fd4fb17-ef7c-4a91-b034-ee8215868d1b ro quiet
        echo    'Loading initial ramdisk ...'
        initrd  /boot/initrd.img-3.16.50so-ciencias-unam
}
```

### Reiniciar el equipo para iniciar con el nuevo *kernel*

Una vez verificado lo anterior se reinicia el equipo

```sh
root@debian:~# reboot
```

### Verificar la versión de *kernel* en el cargador de inicio

Al reiniciar la máquina virtual se muestra la pantalla de `grub`

![Pantalla principal de GRUB en la máquina virtual](img/001-grub_main.png)

Verificar que aparezca el *kernel* que se compiló en la sección de opciones avanzadas

![Pantalla de opciones avanzadas de GRUB](img/002-grub_advanced.png)

Adicionalmente se puede visualizar la configuración al presionar la tecla `e` cuando se resalte alguna opción

![Pantalla de edición de una regla de GRUB](img/003-grub_edit.png)

Selecciona la imágen del *kernel* compilado y presionar `< Enter >` para cargar el sistema operativo

### Verificar la versión de *kernel* cuando se inicie el sistema operativo

#### Verificar utilizando las herramientas del sistema operativo

Una vez que el sistema operativo haya iniciado, verificar la versión del *kernel*

```sh
tonejito@debian:~$ uname -a
Linux debian.local 3.16.50so-ciencias-unam #2 SMP Tue Nov 21 20:24:31 GMT 2017 x86_64 GNU/Linux
```

+ Se puede listar directamente la versión del sistema operativo desde `/proc`

```sh
root@debian:~# cat /proc/version
Linux version 3.16.50so-ciencias-unam (tonejito@debian.local) (gcc version 4.9.2 (Debian 4.9.2-10) ) #2 SMP Tue Nov 21 20:24:31 GMT 2017
```

+ Se puede utilizar el archivo que contiene los argumentos del *kernel*

```sh
root@debian:~# cat /proc/cmdline
BOOT_IMAGE=/boot/vmlinuz-3.16.50so-ciencias-unam root=UUID=2fd4fb17-ef7c-4a91-b034-ee8215868d1b ro quiet
```

+ Se pueden utilizar también los mensajes del *kernel*

```sh
root@debian:~# dmesg | grep $(uname -r)
[    0.000000] Linux version 3.16.50so-ciencias-unam (tonejito@debian.local) (gcc version 4.9.2 (Debian 4.9.2-10) ) #2 SMP Tue Nov 21 20:24:31 GMT 2017
[    0.000000] Command line: BOOT_IMAGE=/boot/vmlinuz-3.16.50so-ciencias-unam root=UUID=2fd4fb17-ef7c-4a91-b034-ee8215868d1b ro quiet
[    0.000000] Kernel command line: BOOT_IMAGE=/boot/vmlinuz-3.16.50so-ciencias-unam root=UUID=2fd4fb17-ef7c-4a91-b034-ee8215868d1b ro quiet
[  128.256512] 00:00:00.001227 main     OS Release: 3.16.50so-ciencias-unam
```

#### Verificar utilizando la información de alguún módulo del *kernel*

Revisar la información de algún módulo para ver la ruta y la versión del *kernel* para la que fue compilado

```sh
root@debian:~# modinfo dummy
filename:       /lib/modules/3.16.50so-ciencias-unam/kernel/drivers/net/dummy.ko
alias:          rtnl-link-dummy
license:        GPL
depends:        
intree:         Y
vermagic:       3.16.50so-ciencias-unam SMP mod_unload modversions
parm:           numdummies:Number of dummy pseudo devices (int)
root@debian:~# ls -la /lib/modules/3.16.50so-ciencias-unam/kernel/drivers/net/dummy.ko
-rw-r--r-- 1 root root 10616 Nov 21 20:30 /lib/modules/3.16.50so-ciencias-unam/kernel/drivers/net/dummy.ko
root@debian:~# file -s /lib/modules/3.16.50so-ciencias-unam/kernel/drivers/net/dummy.ko
/lib/modules/3.16.50so-ciencias-unam/kernel/drivers/net/dummy.ko: ELF 64-bit LSB relocatable, x86-64, version 1 (SYSV), BuildID[sha1]=6723c305915f598e64b5c76e2a16c383b63b1776, not stripped
```

Intentar cargar el módulo y ver si se integra la funcionalidad que contiene

```sh
root@debian:~# modprobe dummy
root@debian:~#
root@debian:~# lsmod | head -n 3
Module                  Size  Used by
dummy                  12717  0
vboxsf                 41363  0
root@debian:~# ifconfig dummy0
dummy0    Link encap:Ethernet  HWaddr 76:f4:57:86:a5:f4
          BROADCAST NOARP  MTU:1500  Metric:1
          RX packets:0 errors:0 dropped:0 overruns:0 frame:0
          TX packets:0 errors:0 dropped:0 overruns:0 carrier:0
          collisions:0 txqueuelen:0
          RX bytes:0 (0.0 B)  TX bytes:0 (0.0 B)
```

#### Listar la ruta de los módulos y cabeceras para el *kernel* compilado

```sh
root@debian:~# cd /lib/modules/3.16.50so-ciencias-unam/
root@debian:/lib/modules/3.16.50so-ciencias-unam# ls -la
total 3756
drwxr-xr-x  4 root root   4096 Nov 21 21:51 .
drwxr-xr-x  4 root root   4096 Nov 21 21:49 ..
drwxr-xr-x 10 root root   4096 Nov 21 21:49 kernel
drwxr-xr-x  3 root root   4096 Nov 21 21:51 updates
lrwxrwxrwx  1 root root     46 Nov 21 20:33 build -> /usr/src/linux-headers-3.16.50so-ciencias-unam
-rw-r--r--  1 root root 913952 Nov 21 21:51 modules.alias
-rw-r--r--  1 root root 885316 Nov 21 21:51 modules.alias.bin
-rw-r--r--  1 root root   3050 Nov 21 20:27 modules.builtin
-rw-r--r--  1 root root   4256 Nov 21 21:51 modules.builtin.bin
-rw-r--r--  1 root root 372476 Nov 21 21:51 modules.dep
-rw-r--r--  1 root root 508908 Nov 21 21:51 modules.dep.bin
-rw-r--r--  1 root root    402 Nov 21 21:51 modules.devname
-rw-r--r--  1 root root 118156 Nov 21 20:27 modules.order
-rw-r--r--  1 root root    210 Nov 21 21:51 modules.softdep
-rw-r--r--  1 root root 443800 Nov 21 21:51 modules.symbols
-rw-r--r--  1 root root 550606 Nov 21 21:51 modules.symbols.bin
root@debian:~# ls -la /usr/src/
total 24
drwxr-xr-x  6 root root 4096 Nov 21 21:49 .
drwxr-xr-x 10 root root 4096 Jun  6 19:23 ..
drwxr-xr-x  4 root root 4096 Nov 16 22:59 linux-headers-3.16.0-4-amd64
drwxr-xr-x  4 root root 4096 Nov 16 22:59 linux-headers-3.16.0-4-common
drwxr-xr-x 23 root root 4096 Nov 21 21:49 linux-headers-3.16.50so-ciencias-unam
drwxr-xr-x  7 root root 4096 Jun  6 19:29 virtualbox-guest-4.3.36
lrwxrwxrwx  1 root root   24 Dec 14  2015 linux-kbuild-3.16 -> ../lib/linux-kbuild-3.16
```

