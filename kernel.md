### Instalación de dependencias

Instalar las dependencias básicas para compilar un *kernel*

```sh
root@debian:~# aptitude install build-essential libncurses5-dev fakeroot
The following NEW packages will be installed:
  build-essential dpkg-dev{a} g++{a} g++-4.9{a} gcc{a} libc6-dev{a} libncurses5-dev
  libstdc++-4.9-dev{a} libtinfo-dev{a} make{a}
0 packages upgraded, 10 newly installed, 0 to remove and 0 not upgraded.
Need to get 0 B of archives. After unpacking 62.6 MB will be used.
Do you want to continue? [Y/n/?] y

	...

Setting up libc6-dev:amd64 (2.19-18+deb8u10) ...
Setting up gcc (4:4.9.2-2) ...
Setting up libstdc++-4.9-dev:amd64 (4.9.2-10) ...
Setting up g++-4.9 (4.9.2-10) ...
Setting up g++ (4:4.9.2-2) ...
update-alternatives: using /usr/bin/g++ to provide /usr/bin/c++ (c++) in auto mode
Setting up make (4.0-8.1) ...
Setting up dpkg-dev (1.17.27) ...
Setting up build-essential (11.7) ...
Setting up libtinfo-dev:amd64 (5.9+20140913-1+b1) ...
Setting up libncurses5-dev:amd64 (5.9+20140913-1+b1) ...
```

### Descargar el código fuente del *kernel* Linux

#### Descargar *tarball* del *kernel*

Entrar al sitio <https://www.kernel.org/> y descargar el archivo *tarball* del *kernel *

```
longterm: 3.16.50  2017-11-11  [tarball] [pgp] [patch] [inc. patch] [view diff] [browse] [changelog]
```

+ <https://cdn.kernel.org/pub/linux/kernel/v3.x/linux-3.16.50.tar.xz>

```sh
tonejito@debian:/mnt$ wget -c 'https://cdn.kernel.org/pub/linux/kernel/v3.x/linux-3.16.50.tar.xz'
--2017-11-14 00:00:00--  https://cdn.kernel.org/pub/linux/kernel/v3.x/linux-3.16.50.tar.xz
Resolving cdn.kernel.org (cdn.kernel.org)... 151.101.49.176, 2a04:4e42:c::432
Connecting to cdn.kernel.org (cdn.kernel.org)|151.101.49.176|:443... connected.
HTTP request sent, awaiting response... 200 OK
Length: 81039536 (77M) [application/x-xz]
Saving to: ‘linux-3.16.50.tar.xz’

linux-3.16.50.tar.xz	100%[========================================>]  77.29M  1.44MB/s  in 53s

2017-11-14 00:01:02 (1.44 MB/s) - ‘linux-3.16.50.tar.xz’ saved [81039536/81039536]
```

#### Descargar la firma GPG

+ <https://cdn.kernel.org/pub/linux/kernel/v3.x/linux-3.16.50.tar.sign>

```sh
tonejito@debian:/mnt$ wget -cq 'https://cdn.kernel.org/pub/linux/kernel/v3.x/linux-3.16.50.tar.sign'
```

Listar para revisar el tamaño de los archivos descargados

```sh
tonejito@debian:/mnt$ ls -lA
total 79160
drwx------ 2 root root    16384 Sep 28 12:00 lost+found
-rw-r--r-- 1 root root 81039536 Nov 14 00:01 linux-3.16.50.tar.xz
-rw-r--r-- 1 root root      833 Nov 14 00:02 linux-3.16.50.tar.sign
```

### Verificar la firma digital del *tarball* del *kernel*

<https://www.kernel.org/signature.html>

#### Descomprimir el *tarball*

```sh
tonejito@debian:/mnt$ unxz linux-3.16.50.tar.xz

tonejito@debian:/mnt$ ls -lA *.tar
-rw-r--r-- 1 root root 579737600 Nov 14 00:10 linux-3.16.50.tar
```

#### Intentar verificar la firma digital con `gpg`

```sh
tonejito@debian:/mnt$ gpg --verify linux-3.16.50.tar.sign
gpg: directory `/home/tonejito/.gnupg' created
gpg: new configuration file `/home/tonejito/.gnupg/gpg.conf' created
gpg: WARNING: options in `/home/tonejito/.gnupg/gpg.conf' are not yet active during this run
gpg: keyring `/home/tonejito/.gnupg/pubring.gpg' created
gpg: assuming signed data in `linux-3.16.50.tar'
gpg: Signature made Mon 13 Nov 2017 02:17:49 AM CST using RSA key ID 6092693E
gpg: Can't check signature: public key not found
```

Este comando falla porque no se tiene el bloque de llaves públicas de los desarrolladores del *kernel*, por lo que será necesario descargarlo:

##### Descargar el bloque de llaves públicas GPG de los desarrolladores del kernel

```sh
tonejito@debian:/mnt$ gpg --keyserver hkp://keys.gnupg.net --recv-keys 6092693E
gpg: keyring `/home/tonejito/.gnupg/secring.gpg' created
gpg: requesting key 6092693E from hkp server keys.gnupg.net
gpg: /home/tonejito/.gnupg/trustdb.gpg: trustdb created
gpg: key 6092693E: public key "Greg Kroah-Hartman (Linux kernel stable release signing key) <greg@kroah.com>" imported
gpg: key 6092693E: public key "Greg Kroah-Hartman (Linux kernel stable release signing key) <greg@kroah.com>" imported
gpg: no ultimately trusted keys found
gpg: Total number processed: 2
gpg:               imported: 2  (RSA: 2)
```

#### Verificar la firma digital con `gpg`

Ejecutar `gpg --verify` por segunda vez para que verifique la firma digital

```sh
tonejito@debian:/mnt$ gpg --verify linux-3.16.50.tar.sign
gpg: assuming signed data in `linux-3.16.50.tar'
gpg: Signature made Mon 13 Nov 2017 02:17:49 AM CST using RSA key ID 6092693E
gpg: Good signature from "Greg Kroah-Hartman (Linux kernel stable release signing key) <greg@kroah.com>"
gpg: WARNING: This key is not certified with a trusted signature!
gpg:          There is no indication that the signature belongs to the owner.
Primary key fingerprint: 647F 2865 4894 E3BD 4571  99BE 38DB BDC8 6092 693E
```

>>>
Estos pasos están cubiertos en el script `get-kernel` ubicado en la siguiente dirección:

+ https://gist.github.com/tonejito/9460148
>>>

### Desempaquetar el *tarball* del *kernel*

Es necesario desempaquetar el archivo tar para acceder al árbol de código fuente del *kernel*

```sh
tonejito@debian:/mnt$ tar -xvf linux-3.16.50.tar
linux-3.16.50/
linux-3.16.50/.gitignore
linux-3.16.50/.mailmap
linux-3.16.50/COPYING
linux-3.16.50/CREDITS
linux-3.16.50/Documentation/
linux-3.16.50/Documentation/.gitignore
linux-3.16.50/Documentation/00-INDEX

	...

linux-3.16.50/virt/kvm/irqchip.c
linux-3.16.50/virt/kvm/kvm_main.c
linux-3.16.50/virt/kvm/vfio.c
```

#### Verificar los archivos extraidos

Verificar que el directorio `linux-3.16.50` tenga el contenido del paquete **tar**:

```sh
tonejito@debian:/mnt$ ls -lA linux-3.16.50
total 588
drwxrwxr-x  31 tonejito users   4096 Nov 11 07:34 arch
drwxrwxr-x   3 tonejito users   4096 Nov 11 07:34 block
drwxrwxr-x   4 tonejito users   4096 Nov 11 07:34 crypto
drwxrwxr-x 106 tonejito users  12288 Nov 11 07:34 Documentation
drwxrwxr-x 117 tonejito users   4096 Nov 11 07:34 drivers
drwxrwxr-x  36 tonejito users   4096 Nov 11 07:34 firmware
drwxrwxr-x  74 tonejito users   4096 Nov 11 07:34 fs
drwxrwxr-x  27 tonejito users   4096 Nov 11 07:34 include
drwxrwxr-x   2 tonejito users   4096 Nov 11 07:34 init
drwxrwxr-x   2 tonejito users   4096 Nov 11 07:34 ipc
drwxrwxr-x  13 tonejito users   4096 Nov 11 07:34 kernel
drwxrwxr-x  11 tonejito users   4096 Nov 11 07:34 lib
drwxrwxr-x   2 tonejito users   4096 Nov 11 07:34 mm
drwxrwxr-x  57 tonejito users   4096 Nov 11 07:34 net
drwxrwxr-x  12 tonejito users   4096 Nov 11 07:34 samples
drwxrwxr-x  13 tonejito users   4096 Nov 11 07:34 scripts
drwxrwxr-x   9 tonejito users   4096 Nov 11 07:34 security
drwxrwxr-x  22 tonejito users   4096 Nov 11 07:34 sound
drwxrwxr-x  18 tonejito users   4096 Nov 11 07:34 tools
drwxrwxr-x   2 tonejito users   4096 Nov 11 07:34 usr
drwxrwxr-x   3 tonejito users   4096 Nov 11 07:34 virt
-rw-rw-r--   1 tonejito users  18693 Nov 11 07:34 COPYING
-rw-rw-r--   1 tonejito users  95930 Nov 11 07:34 CREDITS
-rw-rw-r--   1 tonejito users   1126 Nov 11 07:34 .gitignore
-rw-rw-r--   1 tonejito users   2536 Nov 11 07:34 Kbuild
-rw-rw-r--   1 tonejito users    252 Nov 11 07:34 Kconfig
-rw-rw-r--   1 tonejito users   4762 Nov 11 07:34 .mailmap
-rw-rw-r--   1 tonejito users 283140 Nov 11 07:34 MAINTAINERS
-rw-rw-r--   1 tonejito users  52646 Nov 11 07:34 Makefile
-rw-rw-r--   1 tonejito users  18736 Nov 11 07:34 README
-rw-rw-r--   1 tonejito users   7485 Nov 11 07:34 REPORTING-BUGS
```

#### Copiar la configuración del *kernel* actual

Se hace una copia de la configuración *kernel* actual para tomarla como base para el nuevo *kernel* que vamos a compilar:

```sh
tonejito@debian:/mnt$ cp -v /boot/config-3.16.0-4-amd64 ./linux-3.16.50/.config
‘/boot/config-3.16.0-4-amd64’ -> ‘./linux-3.16.50/.config’

tonejito@debian:/mnt$ ls -la ./linux-3.16.50/.config
-rw-r--r-- 1 tonejito users 157786 Nov 14 00:38 ./linux-3.16.50/.config
```

#### Configurar las opciones del *kernel*

Ejecutar `make menuconfig` para acceder al menú de configuración de las opciones del *kernel*

```sh
tonejito@debian:/mnt$ cd linux-3.16.50
tonejito@debian:/mnt/linux-3.16.50$ make menuconfig
  HOSTCC  scripts/basic/fixdep
  HOSTCC  scripts/kconfig/conf.o
  HOSTCC  scripts/kconfig/lxdialog/checklist.o
  HOSTCC  scripts/kconfig/lxdialog/inputbox.o
  HOSTCC  scripts/kconfig/lxdialog/menubox.o
  HOSTCC  scripts/kconfig/lxdialog/textbox.o
  HOSTCC  scripts/kconfig/lxdialog/util.o
  HOSTCC  scripts/kconfig/lxdialog/yesno.o
  HOSTCC  scripts/kconfig/mconf.o
  SHIPPED scripts/kconfig/zconf.tab.c
  SHIPPED scripts/kconfig/zconf.lex.c
  SHIPPED scripts/kconfig/zconf.hash.c
  HOSTCC  scripts/kconfig/zconf.tab.o
  HOSTLD  scripts/kconfig/mconf
scripts/kconfig/mconf Kconfig
```

##### Pantalla de `make menuconfig`

Una vez que se haya preparado el entorno, se mostrará la interfaz en modo texto para configurar las opciones del *kernel*

```
 .config - Linux/x86 3.16.50 Kernel Configuration
 ──────────────────────────────────────────────────────────────────────────────────────────────────
  ┌────────────────────────── Linux/x86 3.16.50 Kernel Configuration ───────────────────────────┐
  │  Arrow keys navigate the menu.  <Enter> selects submenus ---> (or empty submenus ----).     │  
  │  Highlighted letters are hotkeys.  Pressing <Y> includes, <N> excludes, <M> modularizes     │  
  │  features.  Press <Esc><Esc> to exit, <?> for Help, </> for Search.  Legend: [*] built-in   │  
  │  [ ] excluded  <M> module  < > module capable                                               │  
  │ ┌─────────────────────────────────────────────────────────────────────────────────────────┐ │  
  │ │         [*] 64-bit kernel                                                               │ │  
  │ │             General setup  --->                                                         │ │  
  │ │         [*] Enable loadable module support  --->                                        │ │  
  │ │         [*] Enable the block layer  --->                                                │ │  
  │ │             Processor type and features  --->                                           │ │  
  │ │             Power management and ACPI options  --->                                     │ │  
  │ │             Bus options (PCI etc.)  --->                                                │ │  
  │ │             Executable file formats / Emulations  --->                                  │ │  
  │ │         <M> Intel System On Chip IOSF Sideband support                                  │ │  
  │ │         -*- Networking support  --->                                                    │ │  
  │ │             Device Drivers  --->                                                        │ │  
  │ │             Firmware Drivers  --->                                                      │ │  
  │ │             File systems  --->                                                          │ │  
  │ │             Kernel hacking  --->                                                        │ │  
  │ │             Security options  --->                                                      │ │  
  │ │         -*- Cryptographic API  --->                                                     │ │  
  │ │         [*] Virtualization  --->                                                        │ │  
  │ │             Library routines  --->                                                      │ │  
  │ │                                                                                         │ │  
  │ └─────────────────────────────────────────────────────────────────────────────────────────┘ │  
  ├─────────────────────────────────────────────────────────────────────────────────────────────┤  
  │                  <Select>    < Exit >    < Help >    < Save >    < Load >                   │  
  └─────────────────────────────────────────────────────────────────────────────────────────────┘  
                                                                                                   
```

##### Configurar opciones del *kernel*

Seleccionar la opción `General setup`:

```
  │ │             General setup  --->                                                         │ │  
```

Después seleccionar `Local version`:

```
  │ │               ()  Local version - append to kernel release                              │ │  
```

Aparecerá un cuadro de diálogo donde se pedirá una cadena personalizada que será agregada al final de la versión del *kernel*:

```

            ┌─────────────── Local version - append to kernel release ────────────────┐
            │  Please enter a string value. Use the <TAB> key to move from the input  │  
            │  field to the buttons below it.                                         │  
            │ ┌─────────────────────────────────────────────────────────────────────┐ │  
            │ │so-ciencias-unam                                                     │ │  
            │ └─────────────────────────────────────────────────────────────────────┘ │  
            │                                                                         │  
            ├─────────────────────────────────────────────────────────────────────────┤  
            │                         <  Ok  >      < Help >                          │  
            └─────────────────────────────────────────────────────────────────────────┘  
```

Una vez guardados los cambios, activar la siguiente opción con la barra espaciadora:

```
  │ │               [*] Automatically append version information to the version string        │ │  
```

Seleccionar la opción `Default hostname` para establecer el nombre por defecto del equipo

```
  │ │               ((none)) Default hostname                                                 │ │  
```

Escribir `localhost` en el cuadro de diálogo y aceptar los cambios

```
            ┌─────────────────────────── Default hostname ────────────────────────────┐
            │  Please enter a string value. Use the <TAB> key to move from the input  │  
            │  field to the buttons below it.                                         │  
            │ ┌─────────────────────────────────────────────────────────────────────┐ │  
            │ │localhost                                                            │ │  
            │ └─────────────────────────────────────────────────────────────────────┘ │  
            │                                                                         │  
            ├─────────────────────────────────────────────────────────────────────────┤  
            │                         <  Ok  >      < Help >                          │  
            └─────────────────────────────────────────────────────────────────────────┘  
```

##### Guardar cambios en el archivo `.config`

Seleccionar la opción `< Save >` para guardar los cambios hechos a la configuración del *kernel*

Aceptar el nombre predeterminado (`.config`) en los cuadros de diálogo

```
                      ┌─────────────────────────────────────────────────────┐
                      │  Enter a filename to which this configuration       │  
                      │  should be saved as an alternate.  Leave blank to   │  
                      │  abort.                                             │  
                      │ ┌─────────────────────────────────────────────────┐ │  
                      │ │.config                                          │ │  
                      │ └─────────────────────────────────────────────────┘ │  
                      │                                                     │  
                      ├─────────────────────────────────────────────────────┤  
                      │               <  Ok  >      < Help >                │  
                      └─────────────────────────────────────────────────────┘  
```

```
                    ┌──────────────────────────────────────────────────────────┐
                    │ configuration written to .config                         │  
                    │                                                          │  
                    ├──────────────────────────────────────────────────(100%)──┤  
                    │                         < Exit >                         │  
                    └──────────────────────────────────────────────────────────┘  
```

Al presionar `< Exit >` se mostrará el mensaje que confirma el fin de la configuración de las opciones del *kernel*

```

*** End of the configuration.
*** Execute 'make' to start the build or try 'make help'.

```

#### Visualizar la ayuda de `make`

Ejecutamos `make help` para ver las opciones disponibles

```sh
tonejito@debian:/mnt/linux-3.16.50$ make help
Cleaning targets:
  clean		  - Remove most generated files but keep the config and
                    enough build support to build external modules
  mrproper	  - Remove all generated files + config + various backup files
  distclean	  - mrproper + remove editor backup and patch files

Configuration targets:
  config	  - Update current config utilising a line-oriented program
  nconfig         - Update current config utilising a ncurses menu based program
  menuconfig	  - Update current config utilising a menu based program

	...

Other generic targets:
  all		  - Build all targets marked with [*]
* vmlinux	  - Build the bare kernel
* modules	  - Build all modules

	...

Architecture specific targets (x86):
* bzImage      - Compressed kernel image (arch/x86/boot/bzImage)

	...

Execute "make" or "make all" to build all targets marked with [*]
For further info see the ./README file

```

#### Ejecutar `make all` para compilar el *kernel* y los módulos

Una vez leídas las opciones, ejecutamos `make all` para compilar el *kernel*, los módulos y hacer una imágen comprimida con bzip (**`bzImage`**)

```sh
tonejito@debian:/mnt/linux-3.16.50$ make all
  HOSTLD  scripts/kconfig/conf
scripts/kconfig/conf --silentoldconfig Kconfig
  SYSTBL  arch/x86/syscalls/../include/generated/asm/syscalls_32.h
  SYSHDR  arch/x86/syscalls/../include/generated/asm/unistd_32_ia32.h
  SYSHDR  arch/x86/syscalls/../include/generated/asm/unistd_64_x32.h
  SYSTBL  arch/x86/syscalls/../include/generated/asm/syscalls_64.h
  SYSHDR  arch/x86/syscalls/../include/generated/uapi/asm/unistd_32.h
  SYSHDR  arch/x86/syscalls/../include/generated/uapi/asm/unistd_64.h
  SYSHDR  arch/x86/syscalls/../include/generated/uapi/asm/unistd_x32.h
  HOSTCC  arch/x86/tools/relocs_32.o
  HOSTCC  arch/x86/tools/relocs_64.o
  HOSTCC  arch/x86/tools/relocs_common.o
  HOSTLD  arch/x86/tools/relocs
  CHK     include/config/kernel.release
  UPD     include/config/kernel.release
	...
  CC      init/main.o
  CHK     include/generated/compile.h
  UPD     include/generated/compile.h
  CC      init/version.o
	...
  HOSTCC  usr/gen_init_cpio
  GEN     usr/initramfs_data.cpio.gz
  AS      usr/initramfs_data.o
  LD      usr/built-in.o
  LD      arch/x86/crypto/built-in.o
  CC [M]  arch/x86/crypto/glue_helper.o
	...
  LD [M]  arch/x86/crypto/camellia-aesni-avx2.o
  LD [M]  arch/x86/crypto/serpent-avx2.o
  AS      arch/x86/ia32/ia32entry.o
  CC      arch/x86/ia32/sys_ia32.o
  CC      arch/x86/ia32/ia32_signal.o
  CC      arch/x86/ia32/nosyscall.o
  CC      arch/x86/ia32/syscall_ia32.o
  CC      arch/x86/ia32/ia32_aout.o
  CC      arch/x86/ia32/audit.o
  LD      arch/x86/ia32/built-in.o
  CC      arch/x86/kernel/process_64.o
  CC      arch/x86/kernel/signal.o
	...
  CC [M]  arch/x86/kernel/cpuid.o
  CC [M]  arch/x86/kernel/iosf_mbi.o
  LD      arch/x86/kvm/built-in.o
  CC [M]  arch/x86/kvm/svm.o
  CC [M]  arch/x86/kvm/vmx.o
  CC [M]  arch/x86/kvm/../../../virt/kvm/kvm_main.o
  CC [M]  arch/x86/kvm/../../../virt/kvm/ioapic.o
  CC [M]  arch/x86/kvm/../../../virt/kvm/coalesced_mmio.o
  CC [M]  arch/x86/kvm/../../../virt/kvm/irq_comm.o
  CC [M]  arch/x86/kvm/../../../virt/kvm/eventfd.o
  CC [M]  arch/x86/kvm/../../../virt/kvm/irqchip.o
  CC [M]  arch/x86/kvm/../../../virt/kvm/vfio.o
  CC [M]  arch/x86/kvm/../../../virt/kvm/assigned-dev.o
  CC [M]  arch/x86/kvm/../../../virt/kvm/iommu.o
  CC [M]  arch/x86/kvm/../../../virt/kvm/async_pf.o
  CC [M]  arch/x86/kvm/x86.o
  CC [M]  arch/x86/kvm/mmu.o
	...
  LD      arch/x86/xen/built-in.o
  LD      arch/x86/built-in.o
  CC      kernel/fork.o
  CC      kernel/exec_domain.o
	...
  CC      kernel/crash_dump.o
  CC      kernel/jump_label.o
  LD      kernel/built-in.o
  CC      mm/filemap.o
  CC      mm/mempool.o
  CC      mm/oom_kill.o
	...
  LD      mm/built-in.o
  CC [M]  mm/hwpoison-inject.o
  CC      fs/open.o
  CC      fs/read_write.o
	...
  LD      fs/built-in.o
  CC [M]  fs/binfmt_misc.o
  CC [M]  fs/mbcache.o
  CC      ipc/compat.o
  CC      ipc/util.o
	...
  LD      fs/built-in.o
  CC [M]  fs/binfmt_misc.o
  CC [M]  fs/mbcache.o
  CC      ipc/compat.o
  CC      ipc/util.o
	...
  LD      ipc/built-in.o
  CC      security/apparmor/apparmorfs.o
	...
  LD      security/built-in.o
  CC      crypto/api.o
	...
  CC [M]  crypto/async_tx/async_pq.o
  CC [M]  crypto/async_tx/async_raid6_recov.o
  CC      block/bio.o
	...
  LD      block/built-in.o
	...
  CC      drivers/acpi/tables.o
	...
  LD      drivers/net/built-in.o
  CC [M]  drivers/net/dummy.o
	...
  CC [M]  drivers/xen/xen-acpi-processor.o
  LD      drivers/built-in.o
  LD      sound/built-in.o
	...
  LD [M]  sound/usb/usx2y/snd-usb-us122l.o
  LD      firmware/built-in.o
  CC      arch/x86/pci/i386.o
  CC      arch/x86/pci/init.o
	...
  LD [M]  arch/x86/oprofile/oprofile.o
  CC      net/socket.o
	...
  CC      net/compat.o
  CC      net/sysctl_net.o
  LD      net/built-in.o
  CC      lib/usercopy.o
  CC      lib/lockref.o
	...
  GEN     lib/oid_registry_data.c
  CC [M]  lib/oid_registry.o
  CC      arch/x86/lib/msr-smp.o
  CC      arch/x86/lib/cache-smp.o
	...
  LINK    vmlinux
  LD      vmlinux.o
  MODPOST vmlinux.o
  GEN     .version
  CHK     include/generated/compile.h
  UPD     include/generated/compile.h
  CC      init/version.o
  LD      init/built-in.o
  KSYM    .tmp_kallsyms1.o
  KSYM    .tmp_kallsyms2.o
  LD      vmlinux
  SORTEX  vmlinux
  SYSMAP  System.map
  CC      arch/x86/boot/a20.o
	...
  LD      arch/x86/boot/setup.elf
  OBJCOPY arch/x86/boot/setup.bin
  OBJCOPY arch/x86/boot/vmlinux.bin
  HOSTCC  arch/x86/boot/tools/build
  BUILD   arch/x86/boot/bzImage
Setup is 17404 bytes (padded to 17408 bytes).
System is 3065 kB
CRC d4fc8d84
Kernel: arch/x86/boot/bzImage is ready  (#1)
```

El mensaje anterior indica que se terminó de compilar la parte monolítica del *kernel*.

Como se ejecutó `make all`, la compilación sigue con la segunda parte donde se compilan los módulos:

```
  Building modules, stage 2.
  MODPOST 3046 modules
  CC      arch/x86/crypto/aes-x86_64.mod.o
  LD [M]  arch/x86/crypto/aes-x86_64.ko
	...
  CC      arch/x86/oprofile/oprofile.mod.o
  LD [M]  arch/x86/oprofile/oprofile.ko
  CC      crypto/ablk_helper.mod.o
  LD [M]  crypto/ablk_helper.ko
	...
  CC      crypto/zlib.mod.o
  LD [M]  crypto/zlib.ko
  CC      drivers/acpi/ac.mod.o
  LD [M]  drivers/acpi/ac.ko
	...
  CC      drivers/xen/xenfs/xenfs.mod.o
  LD [M]  drivers/xen/xenfs/xenfs.ko
  CC      fs/9p/9p.mod.o
  LD [M]  fs/9p/9p.ko
	...
  CC      fs/xfs/xfs.mod.o
  LD [M]  fs/xfs/xfs.ko
  CC      lib/bch.mod.o
  LD [M]  lib/bch.ko
	...
  CC      lib/ts_kmp.mod.o
  LD [M]  lib/ts_kmp.ko
  CC      mm/hwpoison-inject.mod.o
  LD [M]  mm/hwpoison-inject.ko
  CC      net/802/garp.mod.o
  LD [M]  net/802/garp.ko
	...
  CC      net/xfrm/xfrm_user.mod.o
  LD [M]  net/xfrm/xfrm_user.ko
  CC      sound/ac97_bus.mod.o
  LD [M]  sound/ac97_bus.ko
	...
  CC      sound/usb/usx2y/snd-usb-usx2y.mod.o
  LD [M]  sound/usb/usx2y/snd-usb-usx2y.ko
  IHEX    firmware/acenic/tg1.bin
  IHEX    firmware/acenic/tg2.bin
	...
  IHEX    firmware/yam/1200.bin
  IHEX    firmware/yam/9600.bin
```

Se puede comprobar que `make` no generó error al revisar el código de salida:

```sh
tonejito@debian:~$ echo $?
0
```

